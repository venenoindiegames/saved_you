extends Area2D

onready var anim = get_node("anim")
onready var scenario = get_parent().get_parent()

func _ready():
	add_to_group(global.ENEMY_GROUP)

func spikes_in():
	anim.play("in")

func spikes_out():
	scenario.spikes_in = false
	anim.play("out")

func _on_spike_body_enter( body ):
	if(body.is_in_group(global.PLAYER_GROUP)):
		body.damage()
		for child in scenario.get_node("spikes").get_children():
			child.spikes_out()
