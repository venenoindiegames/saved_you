extends Node

var scenarios = []
var last_scenario = -1

const CENA_DIR = "res://assets/scenes/scenarios/"

func _ready():
	randomize()
	loadScenarios()


func randomScenario():
	var random_scenario = randi() % scenarios.size()
#	while (random_scenario == last_scenario):
#		random_scenario = randi() % scenarios.size()
#	last_scenario = random_scenario
	return scenarios[random_scenario]

func loadScenarios():
	var dir = Directory.new()
	dir.change_dir(CENA_DIR)
	dir.list_dir_begin()
	
	var scenario_file = dir.get_next()
	
	while scenario_file != "":
		var scenario = load(CENA_DIR + scenario_file)
		if scenario && scenario extends PackedScene:
			scenarios.append(scenario)
		scenario_file = dir.get_next()