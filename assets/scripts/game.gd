extends Node

var main_menu = "res://assets/scenes/main_menu.tscn"
var scenario
onready var paused_menu = get_node("paused_menu/vcontainer")

func _ready():
	set_process(true)
	set_process_input(true)
	instance_scenario()

func _process(delta):
	audio_control.play_music()

func _input(event):
	if(event.is_action_pressed("esc")):
		get_tree().set_pause(!get_tree().is_paused())
		if get_tree().is_paused():
			paused_menu.show()
		else:
			paused_menu.hide()
			

func instance_scenario():
	scenario = load_scenario.randomScenario().instance()
	scenario.set_pos(Vector2())
	get_node("scenarios").add_child(scenario)

func _on_reset_timer_timeout():
	global.player_index = 0
	get_tree().reload_current_scene()


func _on_Menu_pressed():
	get_tree().set_pause(!get_tree().is_paused())
	get_tree().change_scene(main_menu)
	if get_tree().is_paused():
			paused_menu.show()
	else:
			paused_menu.hide()


func _on_resume_pressed():
	get_tree().set_pause(!get_tree().is_paused())
	if get_tree().is_paused():
			paused_menu.show()
	else:
			paused_menu.hide()
