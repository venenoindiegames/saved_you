extends Node

onready var music_player = get_node("music_player")
onready var SFX_player = get_node("SFX_player")

var song1
var song2


func _ready():
	song1 = load("res://assets/audio/music/newbattle.ogg")
	song2 = load("res://assets/audio/music/boss_battle.ogg")
	
func play_music():
	if(!music_player.is_playing()):
		if(music_player.get_stream() == song1):
			music_player.set_stream(song2)
		else:
			music_player.set_stream(song1)
	else: return
	music_player.play()

func play_steps():
	if(!SFX_player.is_active()):
		SFX_player.play("step")
	
func play_damage():
	SFX_player.play("damage")
	
func play_fire():
	SFX_player.play("fire")
	
func play_hook():
	SFX_player.play("hook")

func play_jump():
	SFX_player.play("jump")
	
func play_death():
	SFX_player.play("death")