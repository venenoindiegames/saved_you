extends Node2D

onready var game = get_tree().get_root().get_node("game")

onready var player1_pos = get_node("player1_pos")
onready var player2_pos = get_node("player2_pos")
onready var item1_pos = get_node("item1_pos")
onready var item2_pos = get_node("item2_pos")
onready var spikes = get_node("spikes")

var pre_player = preload("res://assets/scenes/player.tscn")
var pre_item = preload("res://assets/scenes/power_item.tscn")

var spikes_in = false

func _ready():
	instance_players()
	instance_itens()

func instance_players():
	var player1 = pre_player.instance()
	player1.set_pos(player1_pos.get_pos())
	game.add_child(player1)
	var player2 = pre_player.instance()
	player2.set_pos(player2_pos.get_pos())
	game.add_child(player2)

func instance_itens():
	for child in get_node("itens").get_children():
		child.queue_free()
	instance_item1()
	instance_item2()

func instance_item1():
	var item = pre_item.instance()
	item.set_pos(item1_pos.get_pos())
	get_node("itens").add_child(item)

func instance_item2():
	var item = pre_item.instance()
	item.set_pos(item2_pos.get_pos())
	get_node("itens").add_child(item)

func _on_respawn_itens_timeout():
	instance_itens()

func activate_spikes():
	if !spikes_in:
		for child in spikes.get_children():
			child.spikes_in()
		spikes_in = true