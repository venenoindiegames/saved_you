extends Node2D

onready var main_node = get_tree().get_root().get_node("game")
onready var pre_bullet = preload("res://assets/scenes/powers/bullets/invul_bullet.tscn")
onready var bullet_pos = get_node("bullet_pos")

func fire():
	var bullet = pre_bullet.instance()
	bullet.set_pos(bullet_pos.get_global_pos())
	bullet.start_at(get_parent().get_scale())
	main_node.get_node("bullets").add_child(bullet)
	get_parent().get_parent().setup_hook()
	audio_control.play_fire()
	queue_free()