extends Area2D

onready var anim = get_node("anim")
onready var parent = get_parent().get_parent()


var pulled_player
var first = false
var let_go = false
var connected_pulling = false
var connected_pulled = false

signal pulling
signal pulled

func fire():
	audio_control.play_hook()
	anim.play("pull")
	first = true

func _process(delta):
	pulled_player.set_global_pos(get_node("sprite").get_global_pos())

func _on_hook_body_enter( body ):
	if body.is_in_group(global.PLAYER_GROUP) and first:
		if parent.being_pulled == false:
			set_process(true)
			pulled_player = body
			if(!connected_pulling):
				self.connect("pulling", body, "pulling")
				connected_pulling = true
			emit_signal("pulling")
		first = false

func _on_anim_finished():
	set_process(false)
	if(!connected_pulled and pulled_player != null):
		self.connect("pulled", pulled_player, "pulled")
		connected_pulled = true
	emit_signal("pulled")