extends Area2D

var index = 0
onready var tex_power0 = load("res://assets/sprites/powers/itens/power0.png")
onready var tex_power1 = load("res://assets/sprites/powers/itens/power1.png")

onready var invul = preload("res://assets/scenes/powers/invul_gun.tscn")
onready var healing = preload("res://assets/scenes/powers/healing_gun.tscn")

onready var anim = get_node("anim")

var gun_type
var picked = false

signal pick_item

func _ready():
	add_to_group(global.POWER_GROUP)
	index = global.set_item_index()
	if(index == 0):
		healing_gun()
	elif(index == 1):
		invulnerability_gun()
		
func healing_gun():
	get_node("sprite").set_texture(tex_power0)
	gun_type = healing
	
func invulnerability_gun():
	get_node("sprite").set_texture(tex_power1)
	gun_type = invul

func _on_power_item_body_enter( body ):
	if body.is_in_group(global.PLAYER_GROUP) and !picked:
		body.setup_guns(gun_type)
		picked = true
		anim.play("pick")
