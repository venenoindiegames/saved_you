extends KinematicBody2D

const GRAVITY = 1000.0

const WALK_FORCE = 600
const WALK_MIN_SPEED = 10
const WALK_MAX_SPEED = 150
const STOP_FORCE = 1300
const JUMP_SPEED = 350
const JUMP_MAX_AIRBORNE_TIME = 0.2
const FLOOR_ANGLE_TOLERANCE = 40

const SLIDE_STOP_VELOCITY = 1.0
const SLIDE_STOP_MIN_TRAVEL = 1.0

const PULLED_FORCE = 600

var velocity = Vector2()
var on_air_time = 100
var jumping = false

var prev_jump_pressed = false

var new_anim = ""
var cur_anim = ""

export(int, "Player 1", "Player 2") var index = 0

onready var power_pos = get_node("power_pos")
onready var power = get_node("powers")
onready var pre_hook = preload("res://assets/scenes/powers/hook.tscn")

var touching = false
var force
var life = 2
var other_player_pos
var being_pulled = false
var pull_direction = ""
var alive = true
var invulnerability = false
var fire_free = true

func _fixed_process(delta):
	force = Vector2(0, GRAVITY)

	var walk_left = Input.is_action_pressed("move_left" + str(index))
	var walk_right = Input.is_action_pressed("move_right" + str(index))
	var jump = Input.is_action_pressed("jump" + str(index))
	var chao = get_node("ray_right_foot").is_colliding() || get_node("ray_left_foot").is_colliding()
	var stop = true
	
	if(alive):
		if(walk_left and !being_pulled):
			if(velocity.x <= WALK_MIN_SPEED and velocity.x > -WALK_MAX_SPEED):
				force.x -= WALK_FORCE
				stop = false
				get_node("sprite").set_flip_h(true)
				power.set_scale(Vector2(-1,1))
				if (chao and not jumping):
					audio_control.play_steps()
					new_anim = "walk"
		elif(walk_right and !being_pulled):
			if(velocity.x >= -WALK_MIN_SPEED and velocity.x < WALK_MAX_SPEED):
				force.x += WALK_FORCE
				stop = false
				get_node("sprite").set_flip_h(false)
				power.set_scale(Vector2(1,1))
				if (chao and not jumping):
					audio_control.play_steps()
					new_anim = "walk"
	
		if (stop and !being_pulled):
			var vsign = sign(velocity.x)
			var vlen = abs(velocity.x)
			vlen -= STOP_FORCE*delta
			if(vlen < 0):
				vlen = 0
				if (chao and not jumping):
					new_anim = "idle"
			velocity.x = vlen*vsign
			
		if (on_air_time < JUMP_MAX_AIRBORNE_TIME and jump and not prev_jump_pressed and not jumping and !being_pulled):
			velocity.y = -JUMP_SPEED
			jumping = true
			audio_control.play_jump()
			if(!invulnerability):
				new_anim = "jump"

	if(being_pulled):
		pass
	
	velocity += force*delta
	
	var motion = velocity*delta
	
	motion = move(motion)
	
	var floor_velocity = Vector2()
	
	if(is_colliding() and alive):
		var normal = get_collision_normal()
		
		if(rad2deg(acos(normal.dot(Vector2(0, -1)))) < FLOOR_ANGLE_TOLERANCE):
			on_air_time = 0
			floor_velocity = get_collider_velocity()
		
		if(on_air_time == 0 and force.x == 0 and get_travel().length() < SLIDE_STOP_MIN_TRAVEL and abs(velocity.x) < SLIDE_STOP_VELOCITY and get_collider_velocity() == Vector2()):
			revert_motion()
			velocity.y = 0.0
		else:
			motion = normal.slide(motion)
			velocity = normal.slide(velocity)
			move(motion)
	
	if (jumping and velocity.y > 0):
		jumping = false
		if(!chao and alive and !invulnerability):
			new_anim = "fall"
	
	if (velocity.y > 0 and !chao and alive and !invulnerability):
		new_anim = "fall"

	on_air_time += delta
	prev_jump_pressed = jump
	
	if (cur_anim != new_anim):
		get_node("anim").play(new_anim)
		cur_anim = new_anim

	
func pulling():
	being_pulled = true

func pulled():
	being_pulled = false

func set_invul():
	invulnerability = true
	get_node("sprite_shield").show()

func heal():
	if(life < 2):
		life += 1
		update_health()

func damage():
	if(alive and !invulnerability):
		get_tree().get_root().get_node("game").get_node("camera").shake()
		velocity.y = -JUMP_SPEED
		jumping = true
		life -= 1
		new_anim = "invul"
		audio_control.play_damage()
		update_health()
		death()
	elif invulnerability:
		velocity.y = -JUMP_SPEED
		jumping = true
		invulnerability = false
		get_node("sprite_shield").hide()

func update_health():
	get_node("health_bar/health").set_value(life*50)

func death():
	if(life <= 0):
		alive = false
		new_anim = "dead"
		audio_control.play_death()
		get_parent().get_node("reset_timer").start()

func setup_guns(gun):
	if(power.get_child(0) != null):
		power.get_child(0).queue_free()
		var new_gun = gun.instance()
		new_gun.set_pos(power_pos.get_pos())
		power.add_child(new_gun)
	else:
		var hook = pre_hook.instance()
		hook.set_pos(power_pos.get_pos())
		power.add_child(hook)

func setup_hook():
	var hook = pre_hook.instance()
	hook.set_pos(power_pos.get_pos())
	power.add_child(hook)
	
func _input(event):
	if(event.is_action_pressed("attack" + str(index)) and fire_free):
		if(power.get_child(0) != null):
			power.get_child(0).fire()
			fire_free = false
			get_node("fire_timer").start()

func set_texture():
	if(index == 0):
		get_node("sprite").set_texture(global.meat0)
	elif(index == 1):
		get_node("sprite").set_texture(global.meat1)

func _ready():
	setup_guns(pre_hook)
	add_to_group(global.PLAYER_GROUP)
	index = global.set_player_index()
	set_texture()
	set_fixed_process(true)
	set_process_input(true)

func _on_fire_timer_timeout():
	fire_free = true