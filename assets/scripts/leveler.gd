extends Area2D

signal leveler

func _ready():
	self.connect("leveler", get_parent(), "activate_spikes")

func _on_leveler_body_enter( body ):
	if (body.is_in_group(global.PLAYER_GROUP)):
		emit_signal("leveler")
		get_node("anim").play("pull")
