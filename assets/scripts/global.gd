extends Node

const ITENS_DIR = "res://assets/sprites/powers/itens/"

const PLAYER_GROUP = "players"
const ENEMY_GROUP = "enemies"
const POWER_GROUP = "powers"

var player_index = 0
var item_index = 0

onready var meat0 = load("res://assets/sprites/meat0.png")
onready var meat1 = load("res://assets/sprites/meat1.png")

var itens = []

func _ready():
	randomize()
	OS.set_window_size(Vector2(960,800))
	loadItens()

func set_player_index():
	player_index += 1
	return player_index - 1
	
func set_item_index():
	return randi()%itens.size()

func loadItens():
	var dir = Directory.new()
	dir.change_dir(ITENS_DIR)
	dir.list_dir_begin()
	
	var item_file = dir.get_next()
	
	while item_file != "":
		var item = load(ITENS_DIR + item_file)
		if item:
			itens.append(item)
		item_file = dir.get_next()